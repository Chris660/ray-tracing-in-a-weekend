package body Tracer.Types is
   function Origin(R: in Ray) return Vec3 is (R.A);
   function Direction(R: in Ray) return Vec3 is (R.B);
   function Point_At_Parameter(R: in Ray; T: in Float) return Vec3 is
      (R.A + R.B * T);
   function Reflect(Vector: in Vec3; Normal: in Vec3) return Vec3 is
      (Vector - Normal * Dot(Vector, Normal) * 2.0);
end Tracer.Types;
