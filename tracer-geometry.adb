with Tracer.Materials; use Tracer.Materials;
with Tracer.Random;    use Tracer.Random;

with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;

package body Tracer.Geometry is
   function Material(Obj: in Geometric_Object) return Materials.Material_Ptr is
      (Obj.Material);

   function Hit(
      Obj   : in Sphere;
      R     : in Ray;
      T_Min : in Float;
      T_Max : in Float;
      Res   : out Hit_Record)
   return Boolean is
      OC: constant Vec3 := Origin(R) - Obj.Pos;
      A: constant Float := Dot(Direction(R), Direction(R));
      B: constant Float := Dot(OC, Direction(R));
      C: constant Float := Dot(OC, OC) - Obj.Radius ** 2;
      Discriminant: constant Float := B * B - A * C;
      T: Float;
   begin
      if Discriminant > 0.0 then
         T := (-B - Sqrt(Discriminant)) / A;
         if T < T_Max and T > T_Min then
            Res.T      := T;
            Res.P      := Point_At_Parameter(R, Res.T);
            Res.Norm   := (Res.P - Obj.Pos) / Obj.Radius;
            Res.Target := Obj'Unchecked_Access; -- FIXME
            return true;
         end if;

         T := (-B + Sqrt(Discriminant)) / A;
         if T < T_Max and T > T_Min then
            Res.T      := T;
            Res.P      := Point_At_Parameter(R, Res.T);
            Res.Norm   := (Res.P - Obj.Pos) / Obj.Radius;
            Res.Target := Obj'Unchecked_Access; -- FIXME
            return true;
         end if;
      end if;
      return false;
   end Hit;

   function Hit(
      Objs  : in Object_Array;
      R     : in Ray;
      T_Min : in Float;
      T_Max : in Float;
      Res   : out Hit_Record)
   return Boolean is
      Closest: Float := T_Max;
   begin
      return Result : Boolean := false do
         for Obj of Objs loop
            if Hit(Obj.all, R, T_Min, Closest, Res) then
               Result := true; -- hit something!
               Closest := Res.T;
            end if;
         end loop;
      end return;
   end Hit;

   function Random_Unit_Point return Vec3 is
      (Random_Float, Random_Float, Random_Float);

   function Random_Point_In_Unit_Sphere return Vec3 is
   begin
      return P : Vec3 do
         loop
            P := Random_Unit_Point * 2.0 - Vec3'(1.0, 1.0, 1.0);
            exit when Squared_Length(P) < 1.0;
         end loop;
      end return;
   end Random_Point_In_Unit_Sphere;

   function Random_Point_In_Unit_Disc_XY return Vec3 is
   begin
      return P : Vec3 do
         loop
            P := Vec3'(Random_Float, Random_Float, 0.0) * 2.0
               - Vec3'(1.0, 1.0, 0.0);
            exit when Dot(P, P) < 1.0;
         end loop;
      end return;
   end Random_Point_In_Unit_Disc_XY;
end Tracer.Geometry;
