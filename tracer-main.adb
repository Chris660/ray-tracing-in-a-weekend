with Tracer.Renderer;
with Tracer.Geometry;  use Tracer.Geometry;
with Tracer.Materials; use Tracer.Materials;
with Tracer.Random;    use Tracer.Random;
with Tracer.Types;     use Tracer.Types;
with Tracer.Vector;    use Tracer.Vector;
with Ada.Text_IO;      use Ada.Text_IO;
with Ada.Command_Line; use Ada;

-- Main tracer entry point.
procedure Tracer.Main is
   -- Render window parameters.
   Width  : constant := 640;
   Height : constant := 480;
   Aspect_Ratio : constant := Float(Width) / Float(Height);

   -- Define our camera
   Look_From : constant Vec3 := (13.0, 2.0, 3.0);
   Look_At   : constant Vec3 := (0.0, 0.1, 0.0);
   Camera    : constant Renderer.Camera := Renderer.Make_Camera(
      Look_From    => Look_From,
      Look_At      => Look_At,
      View_Up      => (0.0, 1.0, 0.0),
      FOV          => 20.0,
      Aspect_Ratio => Aspect_Ratio,
      Aperture     => 0.08,
      Focus_Distance => 10.0);

   -- The world / scene to render.
   function Random_World(Marble_Count: Natural) return Object_Array is
      -- X/Y Coordinate bounds for our world.
      Coord_Min : constant := -10.0;
      Coord_Max : constant := 10.0;

      -- Sphere sizes
      Marble_Radius : constant := 0.2;
      Static_Radius : constant := 1.0;
      Ground_Radius : constant := 1000.0;

      -- Ground + 3 large static spheres + N marbles.
      World_Size : constant Natural := Marble_Count + 4;

      -- Generates a "marble" with random material properties at `Position`.
      function Random_Marble(Position: in Vec3) return Object_Ptr is
         Selection : constant Float := Random_Float;
         Material  : Material_Ptr;
      begin
         if Selection < 0.8 then
            Material := new Lambertian'(
               Albedo => (
                  Random_Float * Random_Float,
                  Random_Float * Random_Float,
                  Random_Float * Random_Float));
         elsif Selection < 0.95 then
            Material := new Metal'(
               Albedo => (
                  Random_Float * Random_Float,
                  Random_Float * Random_Float,
                  Random_Float * Random_Float),
               Fuzz => 0.5 * Random_Float);
         else
            Material := new Dielectric'(Refractive_Index => 1.5);
         end if;

         return new Geometry.Sphere'(Position, Material, Marble_Radius);
      end Random_Marble;

      Random_Position : Vec3;
   begin
      -- Initialise the RNG
      Reset(223347);

      return World : Object_Array(1..World_Size) do
         -- The ground
         World(1) := new Geometry.Sphere'(
            Pos      => (0.0, -Ground_Radius, 0.0),
            Radius   => Ground_Radius,
            Material => new Lambertian'(Albedo => (0.5, 0.5, 0.5))); -- gray

         -- Three static spheres: Metal, Glass, Diffuse.
         World(2) := new Geometry.Sphere'(
            Pos      => (4.0, Static_Radius, 0.0),
            Radius   => Static_Radius,
            Material => new Metal'(Albedo => (0.7, 0.6, 0.5), Fuzz => 0.0));
         World(3) := new Geometry.Sphere'(
            Pos      => (0.0, Static_Radius, 0.0),
            Radius   => Static_Radius,
            Material => new Dielectric'(Refractive_Index => 1.5));
         World(4) := new Geometry.Sphere'(
            Pos      => (-4.0, Static_Radius, 0.0),
            Radius   => Static_Radius,
            Material => new Lambertian'(Albedo => (0.4, 0.2, 0.1)));

         -- Randomly positioned marbles
         for Index in 5 .. World_Size loop
            -- Avoid collisions with the static spheres
            loop
               Random_Position := (
                  Coord_Min + Random_Float * (Coord_Max - Coord_Min),
                  Marble_Radius,
                  Coord_Min + Random_Float * (Coord_Max - Coord_Min));
               exit when Length(Random_Position - Vec3'(4.0, Marble_Radius, 0.0))
                         >= Static_Radius;
            end loop;

            -- Generate a random marble at this location.
            World(Index) := Random_Marble(Random_Position);
         end loop;
      end return;
   end;

   World       : constant Object_Array := Random_World(400);
   Output_File : File_Type := Standard_Output;
begin
   if Command_Line.Argument_Count = 1 then
      Close(Output_File); -- XXX: is there a better way?
      Create(File => Output_File,
             Mode => Out_File,
             Name => Command_Line.Argument(1));
   end if;
   Renderer.Render_PPM(World, Camera, Width, Height, Output_File);
end Tracer.Main;
