Here are a few sample renders.

Materials - diffuse and metal spheres:
![chapter 9 - materials](ch-9.png)

Glass and refraction - the smaller sphere to the left is solid,
the larger one to the right is a thin glass bubble.
![chapter 10 - glass & refraction](ch-10.png)

Enhanced camera with field-of-view & focus controls:
![chapter 11 - enhanced camera](ch-11.png)

The big picture - over 400 spheres:
![chapter 12 - the big picture](ch-12.png)

NB. the "marbles" are randomly positioned, and the code
does not (yet) prevent collisions.
