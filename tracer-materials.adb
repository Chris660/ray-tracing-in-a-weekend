with Tracer.Geometry; use Tracer.Geometry;
with Tracer.Random;   use Tracer.Random;
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;

package body Tracer.Materials is
   -- Scatter for Lambertians.
   function Scatter(
      Mat          : in Lambertian;
      HR           : in Hit_Record;
      Inbound      : in Ray;
      Scattered    : out Ray;
      Attenuation  : out Vec3)
   return Boolean is
      Target : constant Vec3 := HR.P + HR.Norm + Random_Point_In_Unit_Sphere;
   begin
      Scattered   := (HR.P, Target - HR.P);
      Attenuation := Mat.Albedo;
      return true;
   end Scatter;


   -- Scatter for Metals.
   function Scatter(
      Mat         : in Metal;
      HR          : in Hit_Record;
      Inbound     : in Ray;
      Scattered   : out Ray;
      Attenuation : out Vec3)
   return Boolean is
      Reflected : constant Vec3 := Reflect(Direction(Inbound), HR.Norm);
   begin
      Scattered   := (HR.P, Reflected + Random_Point_In_Unit_Sphere * Mat.Fuzz);
      Attenuation := Mat.Albedo;
      return Dot(Direction(Scattered), HR.Norm) > 0.0;
   end Scatter;


   -- Scatter for Dielectics.
   function Scatter(
      Mat         : in Dielectric;
      HR          : in Hit_Record;
      Inbound     : in Ray;
      Scattered   : out Ray;
      Attenuation : out Vec3)
   return Boolean is
      Reflected      : constant Vec3 := Reflect(Direction(Inbound), HR.Norm);
      Outward_Normal : Vec3;
      Refracted      : Vec3;
      Ni_Over_Nt     : Float;
      Reflect_Prob   : Float;
      Cosine         : Float;

      -- Approximation of the variability of reflection with viewing angle.
      function Schlick(Cosine, Refractive_Index: Float) return Float is
         R0 : constant Float := (
            (1.0 - Refractive_Index) / (1.0 + Refractive_Index)) ** 2;
      begin
         return R0 + (1.0 - R0) * (1.0 - Cosine) ** 5;
      end Schlick;

   begin
      Attenuation := (0.98, 0.98, 0.98);

      if Dot(Direction(Inbound), HR.Norm) > 0.0 then
         -- Internal reflection
         Outward_Normal := -HR.Norm;
         Ni_Over_Nt     := Mat.Refractive_Index;
         Cosine         := Mat.Refractive_Index * Dot(Direction(Inbound), HR.Norm)
                           / Length(Direction(Inbound));
      else
         -- Refracted
         Outward_Normal := HR.Norm;
         Ni_Over_Nt     := 1.0 / Mat.Refractive_Index;
         Cosine         := (-Dot(Direction(Inbound), HR.Norm))
                           / Length(Direction(Inbound));
      end if;

      if Refract(Direction(Inbound), Outward_Normal, Ni_Over_Nt, Refracted) then
         Reflect_Prob := Schlick(Cosine, Mat.Refractive_Index);
      else
         Reflect_Prob := 1.0;
      end if;

      if Random_Float < Reflect_Prob then
         Scattered := (HR.P, Reflected);
      else
         Scattered := (HR.P, Refracted);
      end if;

      return true;
   end Scatter;

   function Refract(
      V, N       : in Vec3;
      Ni_Over_Nt : Float;
      Refracted  : out Vec3)
   return Boolean is
      UV    : constant Vec3  := Unit_Vector(V);
      Dt    : constant Float := Dot(UV, N);
      Discr : constant Float := 1.0 - (Ni_Over_Nt ** 2) * (1.0 - Dt ** 2);
   begin
      if Discr > 0.0 then
         Refracted := (UV - (N * Dt)) * Ni_Over_Nt - N * Sqrt(Discr);
         return true;
      end if;
      return false;
   end Refract;

end Tracer.Materials;
