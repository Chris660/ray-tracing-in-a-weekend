package Tracer.Random is
   -- Return a random value in the range: 0.0 <= x < 1.0
   function Random_Float return Float;

   -- Reset / seed the generator with the value of `Initiator`.
   procedure Reset(Initiator: in Integer);
end Tracer.Random;
