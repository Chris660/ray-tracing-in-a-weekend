-- Common data-types used througout the tracer.
with Tracer.Vector;    use Tracer.Vector;
limited with Tracer.Materials;

package Tracer.Types is
   type Ray is
      record
         A, B: Vec3;
      end record;

   function Origin(R: in Ray) return Vec3;
   function Direction(R: in Ray) return Vec3;
   function Point_At_Parameter(R: in Ray; T: in Float) return Vec3;
   function Reflect(Vector: in Vec3; Normal: in Vec3) return Vec3;

   -- TODO: Colour should be a separate type, but following the book for now...
   subtype Colour is Vec3;

   type Object is interface;
   type Object_Ptr is access constant Object'Class;
   type Object_Array is array (Integer range <>) of Object_Ptr;

   -- Hit_Record stores details about the point where a `Ray` hits an `Object`.
   type Hit_Record is
      record
         T      : Float;
         P      : Vec3;
         Norm   : Vec3;
         Target : Object_Ptr;
      end record;

   function Hit(Obj   : in Object;
                R     : in Ray;
                T_Min : in Float;
                T_Max : in Float;
                Res   : out Hit_Record)
            return Boolean is abstract;

   function Material(Obj : in Object) return Materials.Material_Ptr is abstract;
end Tracer.Types;
