with Tracer.Geometry;  use Tracer.Geometry;
with Tracer.Materials; use Tracer.Materials;
with Tracer.Random;    use Tracer.Random;

with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Ada.Numerics; use Ada.Numerics;

-- TODO: allow to specify output stream
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

package body Tracer.Renderer is
   function Make_Camera(Look_From, Look_At, View_Up: in Vec3;
                        FOV, Aspect_Ratio: in Float;
                        Aperture, Focus_Distance: in Float) return Camera is
      -- FOV is top to bottom in degrees
      Theta       : constant Float := FOV * Pi / 180.0;
      Half_Height : constant Float := Tan(Theta / 2.0);
      Half_Width  : constant Float := Aspect_Ratio * Half_Height;
   begin
      return Cam: Camera do
         Cam.Origin := Look_From;
         Cam.W      := Unit_Vector(Look_From - Look_At);
         Cam.U      := Unit_Vector(Cross(View_Up, Cam.W));
         Cam.V      := Cross(Cam.W, Cam.U);
         
         Cam.Lens_Radius := Aperture / 2.0;
         Cam.Lower_Left  := Look_From - Cam.U * Focus_Distance * Half_Width
                                      - Cam.V * Focus_Distance * Half_Height
                                      - Cam.W * Focus_Distance;
         Cam.Horizontal  := Cam.U * 2.0 * Focus_Distance * Half_Width;
         Cam.Vertical    := Cam.V * 2.0 * Focus_Distance * Half_Height;
      end return;
   end Make_Camera;


   function Get_Ray(Cam: in Camera; S, T: in Float) return Ray is
      Rd    : constant Vec3 := Random_Point_In_Unit_Disc_XY * Cam.Lens_Radius;
      Offset: constant Vec3 := Cam.U * X(Rd) + Cam.V * Y(Rd);
   begin
      return R : Ray do
         R.A := Cam.Origin + Offset;
         R.B := Cam.Lower_Left + Cam.Horizontal * S + Cam.Vertical * T
                - Cam.Origin - Offset;
      end return;
   end Get_Ray;


   procedure Gamma_Correct(Pixel: in out Colour) is
   begin
      Pixel(1) := Sqrt(Pixel(1));
      Pixel(2) := Sqrt(Pixel(2));
      Pixel(3) := Sqrt(Pixel(3));
   end Gamma_Correct;

   -- Background colours
   White    : constant Colour := (1.0, 1.0, 1.0);
   Pale_Blue: constant Colour := (0.4, 0.5, 1.0);


   -- Returns the background colour for Ray `R`.
   function Background(R: in Ray) return Colour is
      Unit_Direction: constant Vec3 := Unit_Vector(Direction(R));
      T: constant Float := 0.5 * (Y(Unit_Direction) + 1.0);
   begin
      return White * (1.0 - T) + Pale_Blue * T;
   end Background;

   -- Limit recursion depth of Render below.
   Max_Render_Depth : constant := 50;

   -- Renders a single pixel by resolving Ray `R`.
   -- If the ray misses ann objects in `World`, returns the background colour.
   function Render(World: in Object_Array;
                   R    : in Ray;
                   Depth: in Positive := 1) return Colour is
      HR : Hit_Record;
   begin
      if Depth < Max_Render_Depth and Hit(World, R, 0.001, Float'Last, HR) then
         declare
            Scattered   : Ray;
            Attenuation : Vec3;
            Hit_Material: constant Material_Ptr := HR.Target.Material;
         begin
            if Hit_Material /= null then
               if Hit_Material.Scatter(HR, R, Scattered, Attenuation) then
                  return Attenuation * Render(World, Scattered, Depth + 1);
               end if;
            end if;
            return (0.01, 0.01, 0.01);
         end;
      else
         return Background(R);
      end if;
   end Render;


   -- Renders a `Width` x `Height` image of `Scene` in PPM format to `File`.
   procedure Render_PPM
      (Scene  : in Object_Array;
       Cam    : in Camera;
       Width  : in Positive;
       Height : in Positive;
       File   : in Ada.Text_IO.File_Type)
   is
      -- Number of rays to cast per output pixel.
      Samples_Per_Pixel: constant := 100;

      function Render(Cam  : in Renderer.Camera;
                      World: in Object_Array;
                      X, Y : in Float) return Colour is
         U, V: Float;
      begin
         return Result : Colour := (0.0, 0.0, 0.0) do
            -- Return the mean of Samples_Per_Pixel samples
            for S in 1 .. Samples_Per_Pixel loop
               U := (X + Random_Float) / Float(Width);
               V := (Y + Random_Float) / Float(Height);
               Result := Result + Render(World, Renderer.Get_Ray(Cam, U, V));
            end loop;
            Result := Result / Float(Samples_Per_Pixel);
            Renderer.Gamma_Correct(Result);
         end return;
      end Render;

      -- Current pixel being rendered.
      Pixel : Colour;

      use Ada.Text_IO;
   begin
      Put_Line(File, "P3");
      Put(File, Width, Width => 0);
      Put(File, " ");
      Put(File, Height, Width => 0);
      New_Line(File);
      Put_line(File, "255");

      -- Render the scene one pixel at a time.
      for Y in 1 .. Height loop
         for X in 1 .. Width loop
            Pixel := Render(Cam, Scene, Float(X - 1), Float(Height - Y));

            Put(File, Integer(R(Pixel) * 255.99), Width => 0);
            Put(File, " ");
            Put(File, Integer(G(Pixel) * 255.99), Width => 0);
            Put(File, " ");
            Put(File, Integer(B(Pixel) * 255.99), Width => 0);
            New_Line(File);
         end loop;
      end loop;
   end Render_PPM;
end Tracer.Renderer;
