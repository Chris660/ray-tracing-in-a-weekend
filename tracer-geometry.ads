with Tracer.Types;  use Tracer.Types;
with Tracer.Vector; use Tracer.Vector;
limited with Tracer.Materials;

-- Geometric objects for rendering.
package Tracer.Geometry is

   type Geometric_Object is abstract new Object with
      record
         Pos: Vec3 := (0.0, 0.0, 0.0);
         Material : access constant Materials.Material'Class;
      end record;

   function Material(Obj: in Geometric_Object) return Materials.Material_Ptr;

   type Sphere is new Geometric_Object with
      record
         Radius: Float := 0.5;
      end record;

   -- Returns true if Ray `R` hits Sphere `Obj` - details stored in `Res`.
   function Hit(Obj   : in Sphere;
                R     : in Ray;
                T_Min : in Float;
                T_Max : in Float;
                Res   : out Hit_Record) return Boolean;

   -- Test all objects in `Objs` for collision with Ray `R`, returns true
   -- if there's at least one collision.
   -- Details of the closest collision (if any) are stored in `Res`.
   function Hit(Objs  : in Object_Array;
                R     : in Ray;
                T_Min : in Float;
                T_Max : in Float;
                Res   : out Hit_Record) return Boolean;

   -- Generate a random point (Vec3) within a unit sphere.
   function Random_Point_In_Unit_Sphere return Vec3;

   -- Generate a random point (Vec3) within a unit disc in the X/Y plane.
   function Random_Point_In_Unit_Disc_XY return Vec3;
end Tracer.Geometry;
