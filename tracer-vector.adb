with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;

package body Tracer.Vector is
   function X(V: in Vec3) return Float is (V(1));
   function Y(V: in Vec3) return Float is (V(2));
   function Z(V: in Vec3) return Float is (V(3));
   function R(V: in Vec3) return Float is (V(1));
   function G(V: in Vec3) return Float is (V(2));
   function B(V: in Vec3) return Float is (V(3));

   -- Arithmetic unary operators
   function "+"(V: in Vec3) return Vec3 is (V);
   function "-"(V: in Vec3) return Vec3 is
      (-V(1), -V(2), -V(3));

   -- Arithmetic binary operators
   function "+"(L, R: in Vec3) return Vec3 is
      (L(1) + R(1), L(2) + R(2), L(3) + R(3));
   function "-"(L, R: in Vec3) return Vec3 is
      (L(1) - R(1), L(2) - R(2), L(3) - R(3));
   function "*"(L, R: in Vec3) return Vec3 is
      (L(1) * R(1), L(2) * R(2), L(3) * R(3));
   function "/"(L, R: in Vec3) return Vec3 is
      (L(1) / R(1), L(2) / R(2), L(3) / R(3));
   function "*"(V: in Vec3; F: Float) return Vec3 is
      (V(1) * F, V(2) * F, V(3) * F);
   function "/"(V: in Vec3; F: Float) return Vec3 is
      (V(1) / F, V(2) / F, V(3) / F);

   -- Vector dot and cross products.
   function Dot(L, R: in Vec3) return Float is
      (L(1) * R(1) + L(2) * R(2) + L(3) * R(3));

   function Cross(L, R: in Vec3) return Vec3 is
      (L(2) * R(3) - L(3) * R(2),
       -(L(1) * R(3) - L(3) * R(1)),
       L(1) * R(2) - L(2) * R(1));

   -- Utility functions
   function Length(V: in Vec3) return Float is
      (Sqrt(Squared_Length(V)));

   function Squared_Length(V: in Vec3) return Float is
      (V(1) * V(1) + V(2) * V(2) + V(3) * V(3));

   function Unit_Vector(V: in Vec3) return Vec3 is
      (V / Length(V));

   procedure Make_Unit(V: in out Vec3) is
      K: constant Float := 1.0 / Length(V);
   begin
      V := V * K;
   end Make_Unit;
end Tracer.Vector;
