package Tracer.Vector is
   type Vec3 is array(1..3) of Float;

   -- Named accessors
   function X(V: in Vec3) return Float with Inline;
   function Y(V: in Vec3) return Float with Inline;
   function Z(V: in Vec3) return Float with Inline;
   function R(V: in Vec3) return Float with Inline;
   function G(V: in Vec3) return Float with Inline;
   function B(V: in Vec3) return Float with Inline;

   -- Arithmetic unary operators
   function "+"(V: in Vec3) return Vec3 with Inline;
   function "-"(V: in Vec3) return Vec3 with Inline;

   -- Arithmetic binary operators
   function "+"(L, R: in Vec3) return Vec3 with Inline;
   function "-"(L, R: in Vec3) return Vec3 with Inline;
   function "*"(L, R: in Vec3) return Vec3 with Inline;
   function "/"(L, R: in Vec3) return Vec3 with Inline;
   function "*"(V: in Vec3; F: Float) return Vec3 with Inline;
   function "/"(V: in Vec3; F: Float) return Vec3 with Inline;

   -- Vector dot and cross products.
   function Dot(L, R: in Vec3) return Float with Inline;
   function Cross(L, R: in Vec3) return Vec3 with Inline;

   -- Utility functions
   function Length(V: in Vec3) return Float with Inline;
   function Squared_Length(V: in Vec3) return Float with Inline;
   function Unit_Vector(V: in Vec3) return Vec3 with Inline;
   procedure Make_Unit(V: in out Vec3) with Inline;
end Tracer.Vector;
