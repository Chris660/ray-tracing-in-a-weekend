with Ada.Numerics.Float_Random; use Ada.Numerics;

package body Tracer.Random is
   -- Random number generator.
   Generator: Float_Random.Generator;

   procedure Reset(Initiator: in Integer) is
   begin
      Float_Random.Reset(Generator, Initiator);
   end Reset;

   function Random_Float return Float is
   begin
      return Float_Random.Random(Generator);
   end Random_Float;
end Tracer.Random;
