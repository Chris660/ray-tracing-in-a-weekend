with Tracer.Types;    use Tracer.Types;
with Tracer.Vector;   use Tracer.Vector;

package Tracer.Materials is
   type Material is abstract tagged null record;
   type Material_Ptr is access constant Material'Class;

   function Scatter(
      Mat         : in Material;
      HR          : in Hit_Record;
      Inbound     : in Ray;
      Scattered   : out Ray;
      Attenuation : out Vec3)
   return Boolean is abstract;

   type Lambertian is new Material with
      record
         Albedo : Vec3;
      end record;

   function Scatter(
      Mat         : in Lambertian;
      HR          : in Hit_Record;
      Inbound     : in Ray;
      Scattered   : out Ray;
      Attenuation : out Vec3)
   return Boolean;

   type Metal is new Material with
      record
         Albedo : Vec3;
         Fuzz   : Float range 0.0 .. 1.0;
      end record;

   function Scatter(
      Mat         : in Metal;
      HR          : in Hit_Record;
      Inbound     : in Ray;
      Scattered   : out Ray;
      Attenuation : out Vec3)
   return Boolean;


   type Dielectric is new Material with
      record
         Refractive_Index : Float := 1.0;
      end record;

   function Scatter(
      Mat         : in Dielectric;
      HR          : in Hit_Record;
      Inbound     : in Ray;
      Scattered   : out Ray;
      Attenuation : out Vec3)
   return Boolean;

   -- Returns `true` if the incident ray `V` is refracted.
   function Refract(
      V, N       : in Vec3;  -- Refract vector V at point N
      Ni_Over_Nt : Float;    -- Incident/transmitted refractive index.
      Refracted  : out Vec3) -- Refracted ray vector - if any.
   return Boolean;

end Tracer.Materials;
