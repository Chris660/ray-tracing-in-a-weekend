with Tracer.Types;    use Tracer.Types;
with Tracer.Vector;   use Tracer.Vector;
with Ada.Text_IO;

package Tracer.Renderer is
   -- Camera abstraction
   type Camera is
      record
         Origin     : Vec3; -- origin of rays
         Lower_Left : Vec3; -- lower left corner of "viewport"
         Horizontal : Vec3; -- viewport "width"
         Vertical   : Vec3; -- viewport "height"

         -- U, V, W are vectors defining the orientation of the camera
         -- relative to the world space.
         U, V, W: Vec3;

         -- A larger radius will increase the depth-of-field blur effect.
         Lens_Radius: Float := 0.05;
      end record;

   -- Create a camera with a given field-of-view (FOV) in degrees.
   -- `Look_From` and `Look_At` are points that define the direction the camera
   -- is "looking", `View_Up` is a vector that controls the cameras rotation in
   -- the view axis - when projected onto the view plane it points "up".
   -- Ch11: `Aperture` and `Focus_Distance` define the diameter of the virtual
   -- aperture, and distance from the "lens" to the focus plane.
   function Make_Camera(Look_From, Look_At, View_Up: in Vec3;
                        FOV, Aspect_Ratio: in Float;
                        Aperture, Focus_Distance: in Float) return Camera;

   -- Return a new Ray at camera screen space coords (S, T).
   function Get_Ray(Cam: in Camera; S, T: in Float) return Ray;

   -- Approximate gamma correction of `Pixel`.
   procedure Gamma_Correct(Pixel: in out Colour);

   -- Renders Scene as viewed by Cam to stdout.
   procedure Render_PPM
      (Scene  : in Object_Array;
       Cam    : in Camera;
       Width  : in Positive;
       Height : in Positive;
       File   : in Ada.Text_IO.File_Type);
end Tracer.Renderer;
